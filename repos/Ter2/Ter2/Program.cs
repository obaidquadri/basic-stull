﻿using System;

namespace Ter2
{    
    enum Months
    {
        January,   
        February,   
        March,      
        April,      
        May,        
        June,       
        July,
        August,
        September = 12,
        October,
        November,
        December,        
    }
        
    class Animal
    {
        public virtual void ansounds()
        {
            Console.WriteLine("the animal makes a sound");
        } 
    }
    class Pig : Animal
    {
        public override void ansounds()
        {
            Console.WriteLine("wee wee");
        }
    }
    class Dog : Animal
    {
        public override void ansounds()
        {
            Console.WriteLine("bark bark");
        }
    }
    class Program
    {
        static void Agelim(int age)
        {
            if (age < 18)
            {
                throw new ArithmeticException("Access denied - You must be at least 18 years old.");
            }
            else
            {
                Console.WriteLine("Welcome, Access Granted");
            }
        }
        static void Main(string[] args)
        {
                        
            Animal an = new Animal();
            Animal dog = new Dog();
            Animal pig = new Pig();

            pig.ansounds();
            dog.ansounds();
            an.ansounds();

            int months = (int)Months.December;
            Console.WriteLine(months);

            Console.WriteLine();

            Agelim(171);

            try
            {
                int[] ranvar = { 1, 2, 3, 4, 22, 5, 6 };
                Console.WriteLine(ranvar[83]);
            }
            catch 
            {
                Console.WriteLine("Error : the file requested is not in the list");
            }
            finally
            {
                Console.WriteLine("Program is successfully executed");
            }
        }
    }
}

/*
    class computer
    {
        private string Name = "superprivate name";
        public string sname
        {
            get { return Name; }
            set { Name = value; }
        }
        public string name;
        public int year;
        public string use;
        public computer(string modelname, int yearn, string usea)
        {
            name = modelname;
            year = yearn;
            use = usea;
        }
    }   
    class Car
    {        
        public string colour = "Red";
        public int Maxspeed = 120;
        public int year = 2002;
        public string model;
        public string name;
                       
        public void full()
        {
            Console.WriteLine("going as fast as it can");
        }
        
    }
    class Vehicle : Car
    {
        public string fsame = "helicopter";
    }
    class Program
    {
        static void NameAge(string name, int age)
        {
            Console.WriteLine("Syed "+name + " is " + age+" years old");
        }

        static void CountM(string country = "Australia")
        {
            Console.WriteLine(country);
        }
      
        static int Integer(int i, int o)
        {
            return i + o;
            
        }
        static void Meth(int x, int y)
        {
            Console.WriteLine(x + y);
            
            Console.WriteLine("New Method which multiplies : "+ x + "x" + y +"="+ x * y );
        }
        static void Main(string[] args)
        {
            
            
            computer keyboard = new computer("Acer", 1976, "keys");
            Console.WriteLine(keyboard.name + " is the name of the company," + " developed in : " +keyboard.year+ " and uses : " +keyboard.use);
            
            Console.WriteLine(keyboard.sname);

            Car suzuki = new Car();
            suzuki.model = "hayabusa";
            Console.WriteLine(suzuki.model);
            suzuki.name = "Suzuki";
            Console.WriteLine(suzuki.name);

            Car air = new Vehicle();
            air.full();
            Console.WriteLine(air.Maxspeed);

            
            int oneninety = 190; 
            int one = 1;
            int tt = 22;
            string swagger;
            char Alphabet = 'A';
            string earth = "Is Earth Round?";
            swagger = "Question..";
            float fl = 3.9F;
            string fullques = swagger + earth; 
            double ninepointnine = 9.9;
            bool earth2 = true; 
            Console.WriteLine(ninepointnine + fl);
            Console.WriteLine(oneninety);
            Console.WriteLine(swagger);
            Console.WriteLine(Alphabet);
            Console.WriteLine(earth);
            Console.WriteLine(earth2);
            Console.WriteLine(fullques);
            Console.WriteLine(oneninety - one + tt);
            Console.WriteLine(Convert.ToString(one)); 

            Console.WriteLine("Enter Username here");
            string username = Console.ReadLine();
            Console.WriteLine("Username Is : " + username);
            Console.WriteLine("Enter Password here");
            string pass = Console.ReadLine();
            Console.WriteLine("Password is : " + pass);
            Console.WriteLine("Age here");
            int age = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Your Age Is : " + age); 

            Console.WriteLine(Math.Max(83,23));
            Console.WriteLine(Math.Sqrt(255));
            Console.WriteLine(Math.Abs(-234));
            Console.WriteLine(Math.Round(9.23));

            string text = "9498409495059505";
            Console.WriteLine("The length of the text string is: " + text.Length);
            string username1 = "obaid96";
            Console.WriteLine(username.ToUpper());
            string firstName = "John";
            string lastName = "Doe";
            string fullname = $"My full name is: {firstName} {lastName}";
            Console.WriteLine(fullname);
            Console.WriteLine(fullname.IndexOf(":"));

          
            string str3 = "John Doe sydney";                   
            int secondhalf = str3.IndexOf("D");
            string LastName = str3.Substring(secondhalf);           
            Console.WriteLine(LastName);5

            string fl2 = "sa";         
            string ninepointnine2 = "sa";
            string WriteLine = $"My name Is :  {ninepointnine}  {fl}  {ninepointnine} ";
            Console.WriteLine(WriteLine);

            int time = 11; 
            string result = (time < 11) ? "Welcome" : "Goodbye";
            Console.WriteLine(result); 

            int day = 7;
            switch (day)
            {
                case 1:
                    Console.WriteLine("Monday");
                    break;
                case 2:
                    Console.WriteLine("Tuesday");
                    break;
                case 3:
                    Console.WriteLine("Wednesday");
                    break;
                case 4:
                    Console.WriteLine("Thursday");
                    break;
                case 5:
                    Console.WriteLine("Friday");
                    break;
                case 6:
                    Console.WriteLine("Saturday");
                    break;
                case 7:
                    Console.WriteLine("Sunday");
                    break;

                    int i = 0;
                    while (i < 5)
                    {
                        Console.WriteLine(i);
                        i++;
                    }
            }



            for (int i = 10; i > 0; i=i-2)
            {
                Console.WriteLine(i);
                
             } 
                          
            Car BMW = new Car();            
            BMW.colour = "blue";
            BMW.Maxspeed = 200;
            BMW.year = 2020;

            Console.WriteLine("BMW's Max Speed is : " + BMW.Maxspeed);
            Console.WriteLine("The Colour is : " + BMW.colour);
            Console.WriteLine("the manufactured year is : " + BMW.year);
            Console.WriteLine(BMW.model);
            Car Volvo = new Car();
            Console.WriteLine("Volvo's Max Speed is : " + Volvo.Maxspeed);
            Console.WriteLine("The Colour is : "+ Volvo.colour);
            Console.WriteLine("the manufactured year is : " + Volvo.year);
            Volvo.full();

            string[] listofcars = {"volvo", "BMW", "Suzuki", "Hyundai"};
            Console.WriteLine(listofcars[0]);

            Meth(43,2);
            int z = Integer(2, 2);
            Console.WriteLine(z);

            CountM("Sweden");
            CountM("India");
            CountM();
            CountM("USA");

            NameAge("Obaid", 23);
            NameAge("Safi", 21);
            NameAge("Saif", 20); 
        }
    }
    }
    */

