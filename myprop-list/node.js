class Property {
  constructor(pname, tenant, adds) {
    this.pname = pname;
    this.tenant = tenant;
    this.adds = adds;
  }
}

class UI {
  static displayProps() {    

    const props = Store.getProps();

    props.forEach((prop) => UI.addPropToList(prop));
  }

  static addPropToList(prop) {
    const list = document.querySelector('#prop-list');
    const row = document.createElement('tr');

    row.innerHTML = `
     <td>${prop.pname}</td>
     <td>${prop.tenant}</td>
     <td>${prop.adds}</td>
     <td><a href="#" class="btn btn-danger brn-sm delete">X</a></td>
    `;

    list.appendChild(row)
        
  }
  static deleteP(el) {
    if (el.classList.contains('delete')) {
      el.parentElement.parentElement.remove();
    }
  }

  static showAlert(msg, className) {
    const div = document.createElement('div');
    div.className = `alert alert-${className}`;
    div.appendChild(document.createTextNode(msg));
    const container = document.querySelector('.container');
    const form = document.querySelector("#prop-form");
    container.insertBefore(div, form);
    setTimeout(() => document.querySelector('.alert').remove(), 2000);

  }

  static clearFields() {
    document.querySelector('#pname').value= '';
    document.querySelector('#adds').value= '';
    document.querySelector('#tenant').value= '';

  }
}

class Store {
  static getProps() {
    let props;
    if (localStorage.getItem('props') === null) {
      props = [];
    } else {
      props = JSON.parse(localStorage.getItem('props'));
    }
    return props;

  }

  static addProp(prop) {
    const props = Store.getProps();
    props.push(prop);

    localStorage.setItem('props', JSON.stringify(props));

  }

  static deleteP(adds) {
    const props = Store.getProps();

    props.forEach((prop, index) => {
      if(prop.adds === adds) {
        props.splice(index, 1);
      }
    });

    localStorage.setItem('props', JSON.stringify(props))

  }
}


document.addEventListener('DOMContentLoaded', UI.displayProps);

document.querySelector('#prop-form').addEventListener('submit', (e) => {
  
  e.preventDefault();
  const pname = document.querySelector('#pname').value;
  const tenant = document.querySelector('#tenant').value;
  const adds = document.querySelector('#adds').value;

  if(pname === "" || tenant === "" || adds === "") {
    UI.showAlert("Please Fill In All Fields", 'danger')
  } else {

  const prop = new Property(pname, tenant, adds);
 

  UI.addPropToList(prop);

  Store.addProp(prop);

  UI.showAlert('Property Added', 'success');

  UI.clearFields();
}

})


document.querySelector('#prop-list').addEventListener('click', (e) => {
  UI.deleteP(e.target)

  Store.deleteP(e.target.parentElement.previousElementSibling.textContent);

  UI.showAlert('Property Removed', 'danger');
});

