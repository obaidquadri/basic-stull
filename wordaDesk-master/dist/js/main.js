window.addEventListener('load', init);

const lvls = {
  easy: 5,
  medium: 3,
  hard: 2,
  ext : 1
};

const currentLvl = lvls.ext;

let time = currentLvl;
let score = 0;
let isPlaying;

const wordInput = document.querySelector('#word-input');
const currentWord = document.querySelector('#current-word');
const scoreDisplay = document.querySelector('#score');
const timeDisplay = document.querySelector('#time');
const message = document.querySelector('#message');
const seconds = document.querySelector('#seconds');

const words = [
  'aegon',
  'drogon',
  'rhaegar',
  'jamie',
  'jon',
  'viserion',
  'winterfell',
  'dothraki',
  'targaryan',
  'stark',
  'lannister',
  'ramsey',
  'bolton',
  'balon',
  'greyjoy',
  'throne',
  'theon',
  'walkers',
  'arya',
  'hound',
  'balerion',
  'rob'
];

function init() {
  seconds.innerHTML = currentLvl;
  showWord(words);
  wordInput.addEventListener('input', startMatch);
  setInterval(countdown, 1000);
  setInterval(checkStatus, 50);
  
}

function startMatch() {
  if(matchWords()) {
    isPlaying = true;
    time = currentLvl + 1;
    showWord(words);
    wordInput.value = '';
    score++;

  }

  if (score === -1) {
    scoreDisplay.innerHTML = 0;
  } else {
   scoreDisplay.innerHTML = score;
  }
}

function matchWords() {
  if(wordInput.value === currentWord.innerHTML) {
    message.innerHTML = 'Correcctt!!';
    return true;
  } else {
    message.innerHTML = '';
    return false;
  }

}

function showWord(words) {
  const randIndex = Math.floor(Math.random() * words.length);
  currentWord.innerHTML = words[randIndex];

}

function countdown() {
  if(time > 0) {
    time--;
  } else if(time === 0) {
    isPlaying = false;
  }
  timeDisplay.innerHTML = time;
  
}

function checkStatus() {
  if (!isPlaying && time === 0) {
    message.innerHTML = "GAME OVEERR!!";
    score = -1;
  }
}
